Lightweight snatch blocks
=========================

These come with zero guarantees regarding strength. Use at your
own risk, and never suspend a person from them nor suspend anything
over a person from them. Do not use them for anything important to
safety, and do not use them in any situation where their failure
could cause harm to person or property.

**These are toys. Any use you make of them is entirely at your
own risk. No engineer has analyzed this design, and even if they
had, their strength would depend on how you made them. These are
published only to show how I made something.**

If you have an actual need for such a
device, I recommend the [SMC CRx Crevasse Rescue
pulley](https://smcgear.com/crx-crevasse-rescue-pulley.html)
(also available [at
REI](https://www.rei.com/product/855700/smc-crx-crevasse-rescue-pulley)
and [at
Amazon](https://www.amazon.com/TRANGO-SMC-CRx-Pulley-Orange/dp/B00F3BX6GQ))
which is advertised to support a person. It's not even expensive.

Feel free to read the [build
log](https://forum.makerforums.info/t/lightweight-snatch-blocks/85903?u=mcdanlj)
which has pictures.
